const increment = () => {
    return {
        type: "increment"
    }
}

const decrement = () => {
    return {
        type: "decrement"
    }
}

const counter = (state=0, action) => {
    console.log(action, state);
    switch(action.type) {
        case "increment":
            return state + 1;
        case "decrement":
            return state - 1;
    }
}
