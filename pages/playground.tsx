import * as React from 'react'
// import 'playground.scss'
import {useState, useEffect} from 'react'
import axios from 'axios'
// import Acc from './Acc'

import {FaFreebsd, FaArchive, FaSpotify, FaBong} from "react-icons/fa";

export default function App() {
  const [text, setText] = useState('');
  const [history, setHistory] = useState([]);

  async function submitHandle() {
    setHistory(prev => [...prev, text]);
    setText('');
    var bot_reply = '';
    const response = await axios.post('http://127.0.0.1:5000/chatbot', {
      user: text
    })
    .then(function (response) {
      bot_reply = response.data.data.bot;
      console.log('sss', bot_reply);
    })
    .catch(function (error) {
      console.log(error);
    });
    console.log('aaa', bot_reply);
    setHistory(prev => [...prev, bot_reply]);
    console.log(history)
  }

  const handleKeypress = e => {
    //it triggers by pressing the enter key
    if (e.key === 'Enter') {
      submitHandle();
    }
  };
  
  return (
    <main>
      <div class="sidebar">
        <ul>
          <li><FaArchive /></li>
          <li><FaFreebsd /></li>
          <li><FaSpotify /></li>
          <li><FaBong /></li>
        </ul>
      </div>
      <div class="main">
        <div class="channel">
          <nav>
            Test Server
          </nav>
          <ul>
            {/* <Acc history={history} /> */}
            <li>kkk</li>
            <li>kkk</li>
            <li>kkk</li>
            <li>Voice Channels</li>
          </ul>
        </div>

        <div class="main-channel">
          <nav>
            Test Server-chatroom
          </nav>
          <div class="flex-main-channel">          
            <div class="main-box">
              <div class="msg-box">
                <ul style={{font: 50}}>
                  {history.map((t, index) => (
                    <li key={index}>{t}</li>
                  ))}
                </ul>
              </div>
              <div class="msg-inp">
                <input 
                  class="msg" 
                  type="text" 
                  value={text}
                  onChange={e => setText(e.target.value)}
                  onKeyPress={handleKeypress}
                />
                <button 
                  class="msg" 
                  onClick={submitHandle}>
                  Send
                </button>
              </div>
            </div>
  
            <div class="acc">
              <ul>
                <li>kkk</li>
                <li>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</li>
                <li>kkk</li>
                <li>kkk</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </main>
  )
}