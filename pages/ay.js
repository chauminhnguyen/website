import { useSelector, useDispatch, Provider } from 'react-redux';

export default function Ay() {

    const counter = useSelector((state) => state.counter);
    const dispatch = useDispatch();
  return (
    <>
        <style jsx>{`
            h1 {
                color: red;
            }

            h2 {
                color: blue;
            }
        `}</style>
        <h1>AY</h1>
        {/* <h2>{store.getState()}</h2>
        {console.log(store.getState())} */}
        <h1>{counter}</h1>
        <button onClick={() => dispatch(increment())}>+</button>
        <button onClick={() => dispatch(decrement())}>-</button>
    </>
  );
}
