from transformers import BlenderbotTokenizer, BlenderbotForConditionalGeneration

tokenizer = BlenderbotTokenizer( "./model/vocab.json", "./model/merges.txt")
model = BlenderbotForConditionalGeneration.from_pretrained("./model")
# facebook/blenderbot-400M-distill"

def generate_conv(user_data):
    new_user_input_ids = tokenizer(user_data, return_tensors='pt')

    chat_history_ids = model.generate(**new_user_input_ids)
    # pretty print last ouput tokens from bot
    response = tokenizer.decode(chat_history_ids[0]).replace("<s>", "").replace("</s>", "")

    return response