# from http.server import BaseHTTPRequestHandler

# class handler(BaseHTTPRequestHandler):
#     def do_GET(self):
#         self.send_response(200)
#         self.send_header('Content-type', 'text/plain')
#         self.end_headers()
#         self.wfile.write(str('Hello World!!').encode())
#         return

from flask import Flask
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from main import generate_conv

app = Flask(__name__)
api = Api(app)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

class ChatBot(Resource):
    def get(self):
        return {'data': "hehe"}, 200  # return data and 200 OK code

    def post(self):
        print("POST request received")
        parser = reqparse.RequestParser()  # initialize
        parser.add_argument('user', required=True)  # add args
        args = parser.parse_args()  # parse arguments to dictionary
        
        print(args)
        new_data = {
            'user': args['user'],
            'bot': generate_conv(args['user'])
        }

        return {'data': new_data}, 200  # return data with 200 OK


api.add_resource(ChatBot, '/chatbot')

if __name__ == '__main__':
    app.run(debug=True)  # run our Flask app